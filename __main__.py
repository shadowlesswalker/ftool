'''
一个处理文件的实用小工具
'''
import os,sys,getopt
import re

class FileTool:
    '''一个工具类，提供一些处理文件的函数。'''
    def renames(root:str='.',newname_pattern:str="{C:02d}{surfix}",original_patterns="(\.\w{1,3})\Z",counter:int=0,test:bool=False):
        '''批量重命名文件。
        root:要批量重命名的目录，其下的所有文件（不包含子文件夹下的文件）都将重命名。
        newname_pattern:新文件名的字符串模板，语法和python的str.format函数一样。
        original_patterns:可选，一个正则表达式，用以从原文件名中提取信息，这些信息可在newname_pattern中做为参数使用。
        counter:计数器初始值，函数内置了一个计数器变量，其值可直接在newname_pattern中使用。
        test:是否测试模式，测试模式下只打印重命名过程信息，但不会真的重命名，可用于测试命令是否正确。
        ----------------------------------------------
        函数内置的其它可在newname_pattern中直接使用的变量如下：
        surfix:原文件后缀名。
        -------------------------------------------------
        示例：
        假设当前目录下有如下文件：
        ./
        ./abc4515.txt
        ./ghtd55.txt
        ./bbrbrb.mp4

        renames('.')  #当前目录下所有文件以数字重命名，保留文件后缀名，结果是：
                        00.txt,01.txt,02.mp4
        
        renames('.','(txt)','my_{C:02d}{surfix}') #所有txt文件重命名为my_xx.txt:
                        00.txt,01.txt,bbrbrb.mp4
        
               
        '''
        if os.path.exists(root)==False or os.path.isdir(root)==False:
            print("!!>>%s is not a dir."%(root))
            return

        if newname_pattern==None:
            newname_pattern="{C:02d}{surfix}"
        if original_patterns==None:
            original_patterns="(\.\w{1,3})\Z"
        
        for fx in os.listdir(root):
            surfix=fx.split('.')  #获取文件后缀名
            if len(surfix)>0:
                surfix='.'+surfix[-1]
            else:
                surfix=""
            
            fxpath=os.path.join(root,fx)
            if os.path.isfile(fxpath)==False:
                break
            args=re.search(original_patterns,fx)
            if args==None:
                print("--match fail for:",fx)
                continue  #如果文件名不配置，跳过该文件
            args=args.groups()
            print('\n"%s" << (*%s,C=%d,surfix="%s")'%(newname_pattern,args,counter,surfix))
            
            
            new_name=newname_pattern.format(*args,C=counter,surfix=surfix)
            try:
                 if test==False: #如果是测试模式，会跳过实际的重命名步骤
                    os.rename(fxpath,os.path.join(root,new_name))
            except Exception as ex:
                print("!!>>%s"%ex)
                return
            print("==>%s-->%s"%(fx,new_name))
            counter=counter+1
        return


def list_lookup(lst,key,key_index=0,value_index=1):
    '''在一个列表中查找匹配的元素的值。
        lst:list，要查找的列表
        key:匹配条件是lst[key_index]==key
        value_index:返回的值是lst[value_index]
        返回值:tuple，(True|False.vaue)
    '''
    if lst==None:
        return (False,None)
    for lx in lst:
        if key==lx[key_index]:            
            return (True,lx[value_index])
    return (False,None)

def main(argv):

    try:
        opts,args=getopt.getopt(argv,"ht",['help','renames='])
    except Exception as ex:
        print("command error,use -h for more help.")
        return
    test=list_lookup(opts,'-t')[0]
    for o,ov in opts:
        if o in ('-h','--help'):
            print("ftool by ShadowlessWalker.")
            print("一个实用的文件处理工具。\n")
            print("-h  #命令帮助")
            print("-t  #测试模式")
            print("--renames rootdir srcPattern dstPattern  #批量重命名文件")
            print("    ftool --renames . (txt) my_{C:02d}w{surfix}  #当前目录下文件名统一重命名为'my_01w.原后缀'的样式")
        elif o in ('--renames'):  #批量重命名文件
            counter=0
            x=list_lookup(opts,'--counter')
            if x[0]:
                counter=x[1]
            original_patterns=None
            newname_pattern=None
            if len(args)>0:
                original_patterns=args[0]
            if len(args)>1:
                newname_pattern=args[1]

            FileTool.renames(ov,newname_pattern=newname_pattern, \
                original_patterns=original_patterns, \
                counter=counter,test=test)
        else:
            pass


if __name__=='__main__':
    main(sys.argv[1:])
