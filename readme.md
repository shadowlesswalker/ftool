# ftool

by ShadowlessWalker

# 简介

ftool(file tool)是一个python小工具，提供了一些批量、灵活处理文件的功能。

# 安装

可以直接下载源码去运行（需要切换目录，稍微麻烦一点）；

推荐下载编译好的 ftool-0.2-py3-none-any.whl 包，使用pip install  ftool-0.2-py3-none-any.whl安装，然后就可以直接运行如下命令：

**python -m ftool 选项和参数**

# 使用教程

#### 批量重命名文件

命令格式：

```bash
ftool [-t] --renames rootdir srcPattern dstPattern
```

**-t**:测试模式，在测试模式下，只输出重命名过程信息，不执行实际重命名操作，可用于检查命令正确与否。

**rootdir**:目录名，该目录下的所有文件将被重命名，当前目录用"."表示。

**srcPattern**:要匹配的原方件名模板，使用[python正则表达式语法](https://docs.python.org/zh-cn/3/library/re.html#regular-expression-syntax)，只有文件名匹配的的文件会被重命名。这个参数的另一个重要作用是可以从原文件名中提取出部分字符串(以()括起来的子表达式的匹配结果将作为参数提供给新文件名)，提取出的子字符串可以用在新文件名中。下面举几个例子：

| 原文件名           | srcPattern     | 意义                                     | 提取出的参数 |
| ------------------ | -------------- | ---------------------------------------- | ------------ |
| abc.S01E01.mp4     | .*             | 匹配任何文件名                           | 无           |
| abc.S01E01.mp4     | (S\d+E\d+)     | 匹配包含SmEn样式的文件名                 | S01E01       |
| abc第1季第15集.mp4 | 第(\d+)第(\d+) | 匹配包含第m季第n集样式的文件名           | 1,15         |
| abc第1季第15集.mp4 | .(..).*第(\d+) | 匹配包含第n样式的文件名，并提取第2-3字符 | bc,15        |

**dstPattern**:新文件名模板，使用python的format格式化语法。模板中可以使用以{}括起来的变量或参数。

例如:my-{0}-{C}-{1}{surfix}，其中：

- {0}表示srcPattern中提取出的第1个参数；

- {1}表示srcPattern中提取出的第2个参数；

- {C}是程序预置的一个计数器变量，每扫描到一个匹配文件，其值自动加1；

- {surfix}是程序预置的一个变量，表示原文件名的后缀部分（包含.）;

    *默认的文件名模板是{C:02}{surfix}，表达以2位数字重命名文件，并保留文件后缀。*

------

假设有以下目录：

>  test/
>
> | ----/abcS01E01def256.mp4
>
> |----/abcdeS01E02def365.mp4
>
> |----/abcfffffffS01E03fiiienifnsifn.mp4
>
> |----/abc.txt

------

把test目录下的所有文件统一重命名为以数字编号的样式：

```python
cd test  #切换到test目录
ftool.py --renames .  #使用了默认参数
```

>  test/      #重命名的结果如下：
>
> | ----/00.mp4
>
> |----/01.mp4
>
> |----/02.mp4
>
> |----/03.txt

------

现在我们只把test目录下的所有mp4文件重命名为以数字编号的样式：

```python
cd test   #切换到test目录
ftool.py --renames . (mp4)
```

 test/    #重命名的结果如下：

| ----/00.mp4

|----/01.mp4

|----/02.mp4

|----/abc.txt     #txt文件名保持不变

------

现在我们来个稍微复杂一点的，把test目录下的所有mp4文件重命名为"abc-S01E01.mp4"的样式：

```python
cd test   #切换到test目录
ftool.py --renames . (...).*(S\d\dE\d\d).*(mp4) {0}-{1}{surfix}
```

 test/    #重命名的结果如下：

| ----/abc-S01E01.mp4

|----/abc-S01E02.mp4

|----/abc-S01E03.mp4

|----/abc.txt     #txt文件名保持不变



